//
//  main.m
//  NLTokenViewTest
//
//  Created by Andrew Lim on 14/10/11.
//  Copyright (c) 2011 No Limit Zone. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NLTokenViewTestAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NLTokenViewTestAppDelegate class]));
    }
}

//
//  NLTokenViewTestViewController.m
//  NLTokenViewTest
//
//  Created by Andrew Lim on 14/10/11.
//  Copyright (c) 2011 No Limit Zone. All rights reserved.
//

#import "NLTokenViewTestViewController.h"

@implementation NLTokenViewTestViewController

@synthesize testTokenView = _testTokenView, testTokenView2 = _testTokenView2;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

    _testTokenView.delegate = self;
    _testTokenView.caption = @"First Test";

    _testTokenView2.delegate = self;
    _testTokenView2.caption = @"これはテストです。";
    _testTokenView2.style = NLTokenViewStyleGray;


}
-(BOOL)tokenViewWillActivate:(NLTokenView *)tokenView {
    switch (tokenView.tag) {
        case 101:
            [_testTokenView2 setState:NLTokenViewStateNormal animated:YES];
            break;
        case 102:
        default:
            [_testTokenView setState:NLTokenViewStateNormal animated:YES];
            break;            
    }
    return YES;
}
-(void)tokenViewDidActivate:(NLTokenView *)tokenView {
    NSString *title, *message, *buttonTitle;
    switch (tokenView.tag) {
        case 101:
            title = @"Yes!";
            message = @"Looks good.";
            buttonTitle = @"Yep";
            break;
        case 102:
        default:
            title = @"ヤッタ〜";
            message = @"これはいいです。";
            buttonTitle = @"うん";
            break;            
    }
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:buttonTitle
                                              otherButtonTitles:nil];
    alertView.tag = tokenView.tag;
    [alertView show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 300) {
        NLTokenView *tokenView;
        switch (buttonIndex) {
            case 1:
                tokenView = _testTokenView2;
//                [_testTokenView setState:NLTokenViewStateNormal animated:YES];
                break;
            case 0:
            default:
                tokenView = _testTokenView;
//                [_testTokenView2 setState:NLTokenViewStateNormal animated:YES];
                break;            
        }
        
        [tokenView setCaption:
         [[alertView textFieldAtIndex:0] text] animated:YES];        
//        [tokenView setState:NLTokenViewStateSelected animated:YES];
    }
    else {
/*        NLTokenView* tokenView = 
            (NLTokenView*) [self.view viewWithTag:alertView.tag];
        [tokenView setState:NLTokenViewStateNormal animated:YES];*/
    }
}

-(IBAction)changeText:(id)sender {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"New Text"
                                                        message:
                              @"Enter text and choose target"
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"First", 
                                                                @"Second", nil];
    [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [[alertView textFieldAtIndex:0] setPlaceholder:@"Type your text here."];
    alertView.tag = 300;
    [alertView show];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

@end

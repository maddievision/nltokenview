//
//  NLTokenViewTestViewController.h
//  NLTokenViewTest
//
//  Created by Andrew Lim on 14/10/11.
//  Copyright (c) 2011 No Limit Zone. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NLTokenView.h"
@interface NLTokenViewTestViewController : 
    UIViewController<NLTokenViewDelegate,UIAlertViewDelegate>

@property (nonatomic, strong) IBOutlet NLTokenView *testTokenView;
@property (nonatomic, strong) IBOutlet NLTokenView *testTokenView2;

-(IBAction)changeText:(id)sender;

@end

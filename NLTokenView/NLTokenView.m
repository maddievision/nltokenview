//
//  NLTokenView.m
//  NLTokenView
//
//  Created by Andrew Lim on 14/10/11.
//  Copyright (c) 2011 No Limit Zone. All rights reserved.
//

#import "NLTokenView.h"

@implementation NLTokenView

@synthesize caption = _caption, state = _state, delegate = _delegate;
@synthesize style = _style;


- (void)adjustFrameToTextContentsAnimated:(BOOL)animated {
    int width = [self.caption 
                 sizeWithFont:[UIFont systemFontOfSize:17.0]].width+18.0;
    
    if (width != _lastWidth) {    
        BOOL dir = (width < _lastWidth);
        _lastWidth = width;
        CGRect frame = self.frame;
        frame.size.width = width;
        frame.size.height = 25.0;
        CGRect labelFrame = CGRectMake(0.0,0.0,frame.size.width,25);
        CGRect imageFrame = CGRectMake(0.0,0.0,frame.size.width,25);

        void (^resizeBlock)() = ^{
            self.frame = frame;
            _imageView.frame = imageFrame;
            _tempImageView.frame = imageFrame;
        };
        
        if (animated) {
            if (dir) {
                _textLabel.text = self.caption;   
                _textLabel.frame = labelFrame;
            }
            [UIView animateWithDuration:0.25f delay:0.0f
                            options:(UIViewAnimationOptionAllowUserInteraction|
                                     UIViewAnimationOptionCurveEaseOut)
                         animations:resizeBlock completion:^(BOOL finished){
                             if (!dir) {
                                 _textLabel.text = self.caption;
                                 _textLabel.frame = labelFrame;   
                             }                         
                         }];
        }
        else {
            resizeBlock();
            _textLabel.frame = labelFrame;
            _textLabel.text = self.caption;
        }
    }


    NSString *imageBaseName;
    switch (self.style) {
        case NLTokenViewStyleGray:
            imageBaseName = @"pill_gray";
            break;
        case NLTokenViewStyleBlue:
        default:
            imageBaseName = @"pill_blue";
            break;
    }
    
    NSString *imageStateName;
    UIColor *fontColor;
    switch (self.state) {
        case NLTokenViewStateSelected:
            imageStateName = @"selected";
            fontColor = [UIColor whiteColor];
            break;
        case NLTokenViewStateNormal:
        default:
            imageStateName = @"normal";
            fontColor = [UIColor blackColor];
            break;
    }
    NSString* imageName = [NSString stringWithFormat:@"%@_%@",
                 imageBaseName, imageStateName];
    if (![imageName isEqualToString:_lastImageName]) {
        _lastImageName = imageName;
        UIImage *image = [[UIImage imageNamed:imageName] 
                      stretchableImageWithLeftCapWidth:12 
                      topCapHeight:12];
        if (!animated) {
            _imageView.image = image;
            _textLabel.textColor = fontColor;
        }
        else {
            _tempImageView.image = _imageView.image;
            _imageView.image = image;
            _tempImageView.hidden = NO;
            _tempImageView.alpha = 1.0;
            [UIView animateWithDuration:0.25f delay:0.0f
                                options:(UIViewAnimationOptionAllowUserInteraction|
                                         UIViewAnimationOptionCurveEaseOut)
                             animations:^{
                                 _tempImageView.alpha = 0.0;
                                 _textLabel.textColor = fontColor;                                 
                             }
                             completion:^(BOOL finished) {
                                 _tempImageView.hidden = YES;
                             }
             
             ];
        }
    }
}
- (void)setStyle:(enum NLTokenViewStyle)style {
    [self setStyle:style animated:NO];
}
- (void)setStyle:(enum NLTokenViewStyle)style animated:(BOOL)animated{
    _style = style;
    [self adjustFrameToTextContentsAnimated:animated];
}
- (void)setState:(enum NLTokenViewState)state {
    [self setState:state animated:NO];
}
- (void)setState:(enum NLTokenViewState)state animated:(BOOL)animated{
    _state = state;
    [self adjustFrameToTextContentsAnimated:animated];
}
- (void)setCaption:(NSString *)caption {
    [self setCaption:caption animated:NO];
}

- (void)setCaption:(NSString *)caption animated:(BOOL)animated{
    _caption = [caption copy];
    [self adjustFrameToTextContentsAnimated:animated];
}
- (void)handleTap:(UITapGestureRecognizer *)sender {
    BOOL willActivate = YES;
    if ([_delegate respondsToSelector:@selector(tokenViewWillActivate:)]) {
        willActivate = [_delegate tokenViewWillActivate:self];                
    }
    if (willActivate) {
        _state = NLTokenViewStateSelected;
        [self adjustFrameToTextContentsAnimated:YES];
        if ([_delegate respondsToSelector:@selector(tokenViewDidActivate:)]) {
            [_delegate tokenViewDidActivate:self];
        }
    }
}

- (void)doSetup {
    _tapGestureRecognizer = [[UITapGestureRecognizer alloc] 
                             initWithTarget:self
                             action:@selector(handleTap:)];
    [self addGestureRecognizer:_tapGestureRecognizer];
    self.autoresizingMask = UIViewAutoresizingNone;
    _state = NLTokenViewStateNormal;
    self.backgroundColor = nil;
    _caption = @"";
    _lastImageName = @"";
    _lastWidth = 0.0;
    _style = NLTokenViewStyleBlue;
    CGRect nullFrame = CGRectMake(0.0,0.0,0.0,0.0);
    
    _imageView = [[UIImageView alloc] initWithFrame:nullFrame];
    _imageView.contentMode = UIViewContentModeScaleToFill;
    [self addSubview:_imageView];        

    _tempImageView = [[UIImageView alloc] initWithFrame:nullFrame];
    _tempImageView.contentMode = UIViewContentModeScaleToFill;
    [self addSubview:_tempImageView];        
    
    _tempImageView.hidden = YES;
    
    _textLabel = [[UILabel alloc] initWithFrame:nullFrame];
    _textLabel.text = self.caption;
    _textLabel.opaque = NO;
    _textLabel.textAlignment = UITextAlignmentCenter;
    _textLabel.backgroundColor = nil;
    [self addSubview:_textLabel];
    [self adjustFrameToTextContentsAnimated:NO];
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self doSetup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self doSetup];
    }
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

//
//  NLTokenView.h
//  NLTokenView
//
//  Created by Andrew Lim on 14/10/11.
//  Copyright (c) 2011 No Limit Zone. All rights reserved.
//

#import <UIKit/UIKit.h>


enum NLTokenViewState {
    NLTokenViewStateNormal,
    NLTokenViewStateSelected
};

enum NLTokenViewStyle {
    NLTokenViewStyleBlue,
    NLTokenViewStyleGray    
};

@class NLTokenView;

@protocol NLTokenViewDelegate <NSObject>
@optional
-(BOOL)tokenViewWillActivate:(NLTokenView*)tokenView;
-(void)tokenViewDidActivate:(NLTokenView*)tokenView;
@end

@interface NLTokenView : UIView {
    UILabel *_textLabel;
    UIImageView *_imageView;
    UIImageView *_tempImageView;
    UITapGestureRecognizer *_tapGestureRecognizer;
    NSString *_lastImageName;
    CGFloat _lastWidth;
}
@property (nonatomic, copy) NSString* caption;
@property (nonatomic, assign) enum NLTokenViewStyle style;
@property (nonatomic, assign) enum NLTokenViewState state;
@property (nonatomic, weak) id<NLTokenViewDelegate> delegate;

- (void)setStyle:(enum NLTokenViewStyle)style animated:(BOOL)animated;
- (void)setState:(enum NLTokenViewState)state animated:(BOOL)animated;
- (void)setCaption:(NSString *)caption animated:(BOOL)animated;


@end
